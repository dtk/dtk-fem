// Version: $Id$
// author IAAGOUBI Ayoub
//

// Commentary:
// use branch feature/asseemble_alternative of dtk-linear-algebra-sparse
// mpirun -np 4 ./bin/dtkFemApp --no-spawn --Nplanes 128 --mesh ../data/gridsize20.vtk --distributedMesh gridsize20distr.vtk --solutionFile solgridsize20.h5 --partitions 4 --logfile console --loglevel trace  --policy mpi3
// to run on nef:
// oarsub -I -l /nodes=1,walltime=3:00:00 -p "cluster='dellc6220a'"
// mpirun -launcher-exec oarsh -f $OAR_NODEFILE -np 4 ./bin/dtkFemApp --no-spawn --Nplanes 128 --mesh ../data/gridsize20.vtk --distributedMesh gridsize20distr.vtk --solutionFile solgridsize20.h5 --partitions 4 --logfile console --loglevel trace  --policy mpi3

// Change Log
//
//

// Code:

#include <QTime>
#include <QFileInfo>
#include <QString>

#include <dtkFemCore>
#include <dtkFftw>

#include <dtkDistributed>
#include <dtkDiscreteGeometryCore>
#include <dtkDistributedMesh.h>

#include <dtkMesh>
#include <dtkMeshData>
#include <dtkMeshReaderGeneric>
#include <dtkIo>

#include <dtkLog>
#include <dtkLinearAlgebraSparse>

#include <iostream>
#include <string>
#include <fstream>
#include <vector>

const double M_PI = 3.1415926535897932384626433832795029;

bool fileExists(const QString& file) {
    QFileInfo checkFile(file);
    // check if file exists and if yes: Is it really a file and no directory?
    return (checkFile.exists() && checkFile.isFile());
}

// /////////////////////////////////////////////////////////////////
// analytic_rhs allows to compute rhs = f(x,y,phi)
// /////////////////////////////////////////////////////////////////
void analytic_rhs(const dtkFemFiniteElementSpace& fes, std::vector<double>& F, dtkDistributedCommunicator *comm, int n_plans){

    double phi;
    double x;
    double y;
    double coslphi;
    double sinmpix;
    double sinnpiy;
    double lphi;
    double deltampix;
    double deltampiy;
    double mpix;
    double npiy;

    long index;
    long plan;
    long i;
    long l;
    long m;
    long n;
    long p;

    long Nx = 32;
    long Ny = 32;
    long lmax = 64;
    long myrank  = comm->rank();

    long my_count = fes.vtc_mapper()->count(myrank);
    long my_first_index = fes.vtc_mapper()->firstIndex(myrank);
    long my_last_index = fes.vtc_mapper()->lastIndex(myrank);
    long dim = my_count * n_plans;

    //fes.coordinates()->rlock();
    F.resize(dim);
    for ( plan = 0; plan < n_plans; ++plan) {
        phi = 2 * M_PI * plan / n_plans;
        for ( i = 0; i < my_count; ++i) {
            p = my_count * plan + i;
            index = 3 * (i + my_first_index);
            x =  fes.coordinates()->at(index);
            y =  fes.coordinates()->at(index + 1);
            deltampix = M_PI * x;
            deltampiy = M_PI * y;
            // Boundary conditions
            if( x == 0.0 || x == 1.0 || y == 0.0 || y == 1.0 ){
                F[p] = 0;
            }else{
                lphi = 0;
                for ( l=1; l<lmax+1; ++l){
                    lphi += phi;
                    coslphi =  std::cos(lphi);
                    mpix =0;
                    for ( m=1; m<Nx+1; ++m){
                        mpix +=deltampix;
                        sinmpix =  std::sin(mpix);
                        npiy = 0;
                        for( n=1; n<Ny+1; ++n){
                            npiy += deltampiy;
                            sinnpiy =std::sin(npiy);
                            F[p] += sinmpix * sinnpiy * coslphi;
                        }
                    }
                }
            }
        }
    }
    //fes.coordinates()->unlock();

}
// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class PoissonRunnable: public QRunnable
{
public:
  QString partitioner_impl;
  int partition_count;
  long n_plans;
  QString meshFile;
  QString distributedMesh;
  QString solutionFile;
public:

  void run(void) {

    dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();
    dtkMeshPartitionMap map;
    dtkMesh *mesh = nullptr;

    // /////////////////////////////////////////////////////////////////
    // // MeshReader
    // /////////////////////////////////////////////////////////////////

    dtkMeshReaderGeneric meshReader;

    meshReader.setMeshFile(meshFile);
    meshReader.run();
    mesh = meshReader.mesh();

    // /////////////////////////////////////////////////////////////////
    // // Partition mesh
    // /////////////////////////////////////////////////////////////////

    dtkMeshPartitioner *partitioner = dtkDiscreteGeometryCore::meshPartitioner::pluginFactory().create(this->partitioner_impl);

    if(!partitioner){
      	dtkError() << "No partitioner instanciated.";
      	exit(0);
    }

    partitioner->setMesh(mesh);
    partitioner->setPartitionCount(this->partition_count);
    partitioner->setMeshPartitionMap(&map);
    partitioner->run();

    // /////////////////////////////////////////////////////////////////
    // // distributed  mesh
    // /////////////////////////////////////////////////////////////////

    dtkDistributedMesh pmesh(mesh, &map);

    long NbElts =  pmesh.cellCount();
    long Nbnodes = pmesh.vertexCount();

    dtkTrace()<<"NbElts="<<NbElts;
    dtkTrace()<<"Nbnodes="<<Nbnodes;

    // /////////////////////////////////////////////////////////////////
    // // Write mesh distributed in a vtk file
    // /////////////////////////////////////////////////////////////////

    comm->barrier();
    if (comm->wid() == 0) {

      std::ofstream fichier(distributedMesh.toStdString().c_str(), std::ios::out | std::ios::trunc);
      if(fichier)
      	{
      	  fichier<< "# vtk DataFile Version 2.0" <<std::endl;
      	  fichier<< distributedMesh.toStdString().c_str() << ", Created by Gmsh" <<std::endl;
      	  fichier<< "ASCII" <<std::endl;
      	  fichier<< "DATASET UNSTRUCTURED_GRID" <<std::endl;
      	  fichier<<"POINTS"<<" "<<Nbnodes<<" "<<"double"<<std::endl;
      	  for(int i=0; i<Nbnodes; ++i){
      	    fichier <<pmesh.coordinates()->at(3*i) <<" "<<pmesh.coordinates()->at(3*i+1) <<" "<< 0<< std::endl;
      	  }
      	  fichier<<" "<<std::endl;
      	  fichier<<"CELLS"<<" "<<NbElts<<" "<< NbElts*4<<std::endl;
      	  for(int i=0; i<NbElts; ++i){
      	    fichier<< "3" <<" "<< pmesh.new_connections_new_vid()->at(3*i)<<" "<< pmesh.new_connections_new_vid()->at(3*i+1)<<" "<< pmesh.new_connections_new_vid()->at(3*i+2) <<std::endl;
      	  }
      	  fichier<<" "<<std::endl;
      	  fichier<<"CELL_TYPES"<<" "<<NbElts<<std::endl;
      	  for(int i=0; i<NbElts; ++i){
      	    fichier<<"5"<<std::endl;
      	  }
      	  fichier.close();
      	}
      else{
      	std::cerr << "Impossible d'ouvrir le fichier !" << endl;
      }
    }

    // /////////////////////////////////////////////////////////////////
    // // Integration points
    // /////////////////////////////////////////////////////////////////

    std::vector<dtkFemIntegrationPoint*> ps;
    ps.resize(3);

    ps[0] = new dtkFemIntegrationPoint(0.5, 0.0, 0.0, 1./3);
    ps[1] = new dtkFemIntegrationPoint(0.0, 0.5, 0.0, 1./3);
    ps[2] = new dtkFemIntegrationPoint(0.5, 0.5, 0.0, 1./3);

    // /////////////////////////////////////////////////////////////////
    // // Finite Element Space
    // /////////////////////////////////////////////////////////////////

    dtkFemFiniteElementSpace fes(pmesh, ps, "P1");

    // /////////////////////////////////////////////////////////////////
    // // define mapper vertex to cell
    // /////////////////////////////////////////////////////////////////

    long myrank  = comm->rank();
    long n_procs = comm->size();

    long my_count = fes.vtc_mapper()->count(myrank);
    long my_first_index = fes.vtc_mapper()->firstIndex(myrank);
    long my_last_index = fes.vtc_mapper()->lastIndex(myrank);

    long dim = my_count * n_plans;

    // /////////////////////////////////////////////////////////////////
    // // define BiLinear Form
    // // M : Mass matrix
    // // K : Stifness matrix
    // // MK = K + (plan)² * M
    // /////////////////////////////////////////////////////////////////

    QString vectorImpl = "dtkDistributedVectorData";
    QString matrixImpl = "dtkDistributedSparseMatrixEngineCSR";

    dtkDistributedGraphTopology *matrix_graph = new dtkDistributedGraphTopology(Nbnodes,fes.vtc_mapper());
    dtkSparseMatrix<double> *MK = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);
    dtkSparseMatrix<double> *K;
    dtkSparseMatrix<double> *M;

    dtkFemBilinearForm *A = new dtkFemBilinearForm(fes);
    dtkFemMassTerm mass;
    dtkFemDiffusionTerm stiff;

    A->addTerm(mass,"u");
    A->addTerm(stiff,"-Delta . u");
    A->assemble();
    comm->barrier();

    M = A->matrix("u");
    K = A->matrix("-Delta . u");
    MK->setGraph(matrix_graph);

    M->assemble();
    K->assemble();
    // // /////////////////////////////////////////////////////////////////
    // // Boundary conditions
    // // /////////////////////////////////////////////////////////////////



    for (long i= 0; i<Nbnodes; ++i){
        if((pmesh.coordinates()->at(3*i) == 0 || pmesh.coordinates()->at(3*i+1) == 0 || pmesh.coordinates()->at(3*i) == 1 || pmesh.coordinates()->at(3*i+1)== 1) ){
                for (long j= 0; j<Nbnodes; ++j){
                    K->setAt(i, j, 0);
                    M->setAt(i, j, 0);
                    if(i==j){
                        K->setAt(i, j, 1);
                        M->setAt(i, j, 1);
                    }
                }
        }
    }


    // /////////////////////////////////////////////////////////////////
    // // Linear Form b corresponds to the rhs
    // /////////////////////////////////////////////////////////////////

    dtkFemLinearForm *b = new dtkFemLinearForm(fes);
    std::vector<double> F;

    analytic_rhs(fes, F, comm, n_plans);

    // /////////////////////////////////////////////////////////////////
    // // rhs to rhs_spectral <=> FftForward
    // /////////////////////////////////////////////////////////////////

    fftForward *fftFor = new fftForward(n_plans, my_count);
    std::vector<double> rhs_spectral(dim);
    fftFor->setData(F, rhs_spectral);
    fftFor->run();

    // /////////////////////////////////////////////////////////////////
    // // Solve for each plan
    // /////////////////////////////////////////////////////////////////

    dtkSparseSolver<double> *solver = dtkLinearAlgebraSparse::solverFactory::create<double>("hypreSparseSolverPCG");

    if(! solver){
        qWarning()<<"solver not found";
        return ;
    }

    dtkVector<double> *bsolv = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);
    bsolv->resize(Nbnodes);
    bsolv->setMapper(fes.vtc_mapper());

    std::vector<double> Xs(dim);
    dtkVector<double> *rhs;

    // Cf. Algofftdistributed --> ../../doc/Algo_fftdistributed.pdf
    for (std::size_t plan = 0; plan < n_plans; ++plan) {

        memcpy(bsolv->data(), rhs_spectral.data() + (plan * my_count), my_count * sizeof(double));
        comm->barrier();

        dtkFemDomainIntegratorTerm *d = new dtkFemDomainIntegratorTerm(bsolv);

        b->addTerm(d, "f(x,y,phi)");
        b->assemble();
        rhs = b->rhs("f(x,y,phi)");

        *MK = *M;
        *MK *= plan*plan;
        *MK += *K;

        comm->barrier();

        solver->setMatrix(MK);
        solver->setRHSVector(rhs);
        QHash<QString, int> parameters_int;
        parameters_int.insert("number_of_iterations", 500);
        solver->setOptionalParameters(parameters_int);

        QHash<QString, double> parameters_real;
        parameters_real.insert("residual_reduction_order", 1.e-10);
        solver->setOptionalParameters(parameters_real);

        solver->run();

        dtkVector<double>& sol = *solver->solutionVector();
        memcpy(Xs.data() + (plan * my_count), sol.data(), my_count * sizeof(double));

    }

    // // // /////////////////////////////////////////////////////////////////
    // // sol_spectral to sol <=> FftBackward
    // // // /////////////////////////////////////////////////////////////////

    fftBackward *fftBack=new fftBackward(n_plans, my_count);
    std::vector<double> solf(dim );
    fftBack->setData(Xs, solf);
    fftBack->run();

    // // // /////////////////////////////////////////////////////////////////
    // // write solution HDF5
    // // // /////////////////////////////////////////////////////////////////

    dtkIoDataModel *data_model = dtkIo::dataModel::pluginFactory().create("pHdf5IoDataModel");

    if(!data_model){
        dtkFatal()<<"FAtal error";
    }
    comm->barrier();
    data_model->setCommunicator(comm);
    DTK_DISTRIBUTED_BEGIN_GLOBAL
        if(fileExists(solutionFile)) {
            QFile::remove(solutionFile);
        }
    DTK_DISTRIBUTED_END_GLOBAL

        data_model->fileOpen(solutionFile, dtkIoDataModel::Trunc);

    quint64 shape_global[1] = {quint64(Nbnodes)};
    quint64 shape_local[1] = {quint64(my_count)};
    quint64 shape_offset[1] = {quint64(my_first_index)};
    for (std::size_t p = 0; p < n_plans; ++p) {
        QString dataset_name =  QString("/dataByCoord%1").arg(p,3,10,QChar('0'));
        data_model->write(dataset_name, dtkIoDataModel::Double, 1, shape_global);
        double * offset = solf.data() + (p * my_count);
        data_model->writeHyperslab(dataset_name, dtkIoDataModel::Double, shape_offset,NULL, shape_local, NULL,shape_local,offset);

    }
    data_model->fileClose();

  }

};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
  dtkDistributedApplication *app = dtkDistributed::create(argc, argv);
  app->setApplicationName("poisson  prototype");
  app->setApplicationVersion("0.0.1");

  QCommandLineParser *parser = app->parser();

  QCommandLineOption option_Nplanes("Nplanes", "Number of planes", "Nplanes_value");
  parser->addOption(option_Nplanes);

  QCommandLineOption option_mesh("mesh", "Mesh file", "mesh_value");
  parser->addOption(option_mesh);

  QCommandLineOption option_distributedMesh("distributedMesh", "distributed Mesh file", "distributedMesh_value");
  parser->addOption(option_distributedMesh);

  QCommandLineOption option_solutionFile("solutionFile", "Solution file hdf5", "solutionfile_value");
  parser->addOption(option_solutionFile);

  QCommandLineOption partitionOption("partitions", "number of partitions", "integer", "1");
  parser->addOption(partitionOption);

  QCommandLineOption partitionerImplOption("partitioner", "partitioner implementation. ", "metis|scotch|...", "metis");
  parser->addOption(partitionerImplOption);

  QCommandLineOption showPartitionersOption("show-partitioners", "show partitioner implementations available.");
  parser->addOption(showPartitionersOption);

  app->initialize();

  // ------------ initialize layers

  dtkFemCore::initialize();
  QCommandLineOption verboseOption("verbose", QCoreApplication::translate("main", "verbose plugin initialization"));
  if (parser->isSet(verboseOption)) {
    dtkFemCore::setVerboseLoading(true);
  }

  QStringList partitioners = dtkDiscreteGeometryCore::meshPartitioner::pluginFactory().keys();
  QString partitionersStr = partitioners.filter(QRegExp("^(?!.*Generic).*$")).join(" ");

  if (parser->isSet(showPartitionersOption)) {
    qDebug() << " dynamically loaded partitioners:" << partitionersStr;
    exit(0);
  }
  dtkInfo() << "Dynamicaly loaded partitioner plugins:" << partitionersStr;

  // ------------ check parameters

  if (!parser->isSet(option_mesh) || !parser->isSet(partitionOption)) {
    qCritical() << "Error: no mesh or number of partition were set ! Use --mesh <filename> --partition <integer>" ;
    return 1;
  }

  qWarning()<< parser->value(option_Nplanes)<< parser->value(option_mesh);

  PoissonRunnable PoissonRun;

  // ----- Sets the partitioner

  if (parser->isSet(partitionerImplOption)) {
    if (parser->value(partitionerImplOption) == "metis") {
      PoissonRun.partitioner_impl = "dtkMeshPartitionerMetis";
    } else {
      qWarning() << "Unknown partitioner " << parser->value(partitionerImplOption) << "abort";
      return 1;
    }
  } else {
    PoissonRun.partitioner_impl = "dtkMeshPartitionerMetis";
  }

  if (!partitionersStr.contains(PoissonRun.partitioner_impl)) {
    qWarning() << "Partitioner " << PoissonRun.partitioner_impl << "is not available. Please check your plugin path. Abort.";
    return 1;
  }

  PoissonRun.n_plans = parser->value(option_Nplanes).toInt();
  PoissonRun.meshFile  = parser->value(option_mesh);
  PoissonRun.distributedMesh  = parser->value(option_distributedMesh);
  PoissonRun.solutionFile  = parser->value(option_solutionFile);

  PoissonRun.partition_count = parser->value(partitionOption).toInt();

  app->spawn();
  app->exec(&PoissonRun);
  app->unspawn();

  return 0 ;

}

//
// main.cpp ends here

// Version: $Id$
//
//

// Commentary:
// oarsub -I -l /nodes=2 -p "cluster='dellc6220a'"
// mpirun -launcher-exec oarsh -f $OAR_NODEFILE
// mpirun -np 4 ./bin/dtkFemApp --no-spawn --Nplanes 128 --mesh ../data/gridsize20.vtk --distributedMesh gridsize20distr.vtk --solutionFile solgridsize20.h5 --partitions 4 --logfile console --loglevel trace  --policy mpi3

// Change Log
//
//

// Code:

#include <QtCore>

#include <dtkFemCore>
#include <dtkFftw>

#include <dtkDistributed>
#include <dtkDiscreteGeometryCore>
#include <dtkDistributedMesh.h>

#include <dtkMesh>
#include <dtkMeshData>
#include <dtkMeshReaderGeneric>
#include <dtkIo>

#include <dtkLog>
#include <dtkLinearAlgebraSparse>

#include <iostream>
#include <string>
#include <fstream>
#include <vector>

// ///////////////////////////////////////////////////////////////////

std::size_t operator "" _sz (unsigned long long int x)
{
    return x;
}

// ///////////////////////////////////////////////////////////////////

static bool fileExists(QString file) {
    QFileInfo checkFile(file);
    // check if file exists and if yes: Is it really a file and no directory?
    if (checkFile.exists() && checkFile.isFile()) {
        return true;
    } else {
        return false;
    }
}
// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class PoissonRunnable: public QRunnable
{
public:
  QString partitioner_impl;
  int partition_count;
  long n_plans;
  QString meshFile;
  QString distributedMesh;
  QString solutionFile;
public:

  void run(void) {

    dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();
    dtkMeshPartitionMap map;
    dtkMesh *mesh = nullptr;

    // /////////////////////////////////////////////////////////////////
    // // MeshReader
    // /////////////////////////////////////////////////////////////////

    //dtkMeshReader* meshReader = dtkDiscreteGeometryCore::meshReader::pluginFactory().create("vtkMeshReader");

    dtkMeshReaderGeneric meshReader;

    // if(!meshReader){
    //   std::cout<<"no Reader found"<<std::endl;
    //   exit(0);
    // }
    meshReader.setMeshFile(meshFile);
    meshReader.run();
    mesh = meshReader.mesh();


    // /////////////////////////////////////////////////////////////////
    // // Partition mesh
    // /////////////////////////////////////////////////////////////////

    // dtkMeshPartitioner *partitioner = dtkDiscreteGeometryCore::meshPartitioner::pluginFactory().create(this->partitioner_impl);

    //   if(!partitioner){
    //   	dtkError() << "No partitioner instanciated.";
    //   	exit(0);
    //   }

    //   partitioner->setMesh(mesh);
    //   partitioner->setPartitionCount(this->partition_count);
    //   partitioner->setMeshPartitionMap(&map);
    //   partitioner->run();
    map.read("/home/aiaagoub/development/fftdistributed/build/map_gridsize40.txt");

    // /////////////////////////////////////////////////////////////////
    // // distributed  mesh
    // /////////////////////////////////////////////////////////////////

    dtkDistributedMesh pmesh(mesh, &map);

    // /////////////////////////////////////////////////////////////////
    // // nodes and elements
    // /////////////////////////////////////////////////////////////////

    long NbElts =  pmesh.cellCount();
    long Nbnodes = pmesh.vertexCount();
    long myrank  = comm->rank();
    long n_procs = comm->size();

    int topo_dim = pmesh.topologicalDimension();
    dtkDistributedGraphTopology *vtc_graph =  pmesh.connectivity(0, topo_dim).graph();

    long my_count = vtc_graph->mapper()->count(myrank);
    long my_first_index = vtc_graph->mapper()->firstIndex(myrank);
    long my_last_index = vtc_graph->mapper()->lastIndex(myrank);
    long dim = my_count * n_plans;
    // pmesh.connectivity(pmesh.topologicalDimension(), 0).setGraph(graph);

    // dtkDistributedMapper mapper ;
    // mapper.setMapping(Nbnodes, n_procs);



    // long my_count = mapper.count(myrank);
    // long my_first_index = mapper.firstIndex(myrank);
    // long my_last_index = mapper.lastIndex(myrank);



    dtkTrace()<<"NbElts="<<NbElts<<my_first_index<<my_last_index<<my_count;
    dtkTrace()<<"Nbnodes="<<Nbnodes;

    // /////////////////////////////////////////////////////////////////
    // // Write mesh distributed
    // /////////////////////////////////////////////////////////////////

    comm->barrier();
    if (comm->wid() == 0) {

      std::ofstream fichier(distributedMesh.toStdString().c_str(), std::ios::out | std::ios::trunc);
      if(fichier)
      	{
      	  fichier<< "# vtk DataFile Version 2.0" <<std::endl;
      	  fichier<< distributedMesh.toStdString().c_str() << ", Created by Gmsh" <<std::endl;
      	  fichier<< "ASCII" <<std::endl;
      	  fichier<< "DATASET UNSTRUCTURED_GRID" <<std::endl;
      	  fichier<<"POINTS"<<" "<<Nbnodes<<" "<<"double"<<std::endl;
      	  for(int i=0; i<Nbnodes; ++i){
      	    fichier <<pmesh.coordinates()->at(3*i) <<" "<<pmesh.coordinates()->at(3*i+1) <<" "<< 0<< std::endl;
      	  }
      	  fichier<<" "<<std::endl;
      	  fichier<<"CELLS"<<" "<<NbElts<<" "<< NbElts*4<<std::endl;
      	  for(int i=0; i<NbElts; ++i){
      	    fichier<< "3" <<" "<< pmesh.new_connections_new_vid()->at(3*i)<<" "<< pmesh.new_connections_new_vid()->at(3*i+1)<<" "<< pmesh.new_connections_new_vid()->at(3*i+2) <<std::endl;
      	  }
      	  fichier<<" "<<std::endl;
      	  fichier<<"CELL_TYPES"<<" "<<NbElts<<std::endl;
      	  for(int i=0; i<NbElts; ++i){
      	    fichier<<"5"<<std::endl;
      	  }
      	  fichier.close();
      	}
      else{
      	std::cerr << "Impossible d'ouvrir le fichier !" << endl;
      }
    }



    // /////////////////////////////////////////////////////////////////
    // // Integration points
    // /////////////////////////////////////////////////////////////////

    std::vector<dtkFemIntegrationPoint*> ps;
    ps.resize(3);

    ps[0] = new dtkFemIntegrationPoint(0.5, 0.0, 0.0, 1./3);
    ps[1] = new dtkFemIntegrationPoint(0.0, 0.5, 0.0, 1./3);
    ps[2] = new dtkFemIntegrationPoint(0.5, 0.5, 0.0, 1./3);

    // /////////////////////////////////////////////////////////////////
    // // Finite Element Space
    // /////////////////////////////////////////////////////////////////

    //dtkFiniteElementSpace fes(pmesh,ps,"P1");
    dtkFemFiniteElementSpace fes(pmesh, ps, "P1");

    // /////////////////////////////////////////////////////////////////
    // // BiLinear Form
    // /////////////////////////////////////////////////////////////////

    dtkFemBilinearForm *A = new dtkFemBilinearForm(fes);
    dtkFemMassTerm mass;
    dtkFemDiffusionTerm stiff;

    A->addTerm(mass,"u");
    A->addTerm(stiff,"-Delta . u");

    A->assemble();

    QString vectorImpl = "dtkDistributedVectorData";
    QString matrixImpl = "dtkDistributedSparseMatrixEngineCSR";

    dtkSparseMatrix<double> *MK = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);
    dtkSparseMatrix<double> *K;
    dtkSparseMatrix<double> *M;

    MK->resize(Nbnodes);


    M = A->matrix("u");
    K = A->matrix("-Delta . u");

    // // /////////////////////////////////////////////////////////////////
    // // Condition aux limites
    // // /////////////////////////////////////////////////////////////////

    pmesh.coordinates()->rlock();
    for (long i= 0; i<Nbnodes; ++i){
        // if( i >= my_first_index && i<= my_last_index){
            if((pmesh.coordinates()->at(3*i) == 0 || pmesh.coordinates()->at(3*i+1) == 0 || pmesh.coordinates()->at(3*i) == 1 || pmesh.coordinates()->at(3*i+1)== 1) ){
                for (long j= 0; j<Nbnodes; ++j){
                    K->remove(i, j);
                    M->remove(i, j);
                    if(i==j){
                        K->insert(i, j, 1./n_procs);
                        M->insert(i, j, 1./n_procs);
                    }
                }
            }
        }

    pmesh.coordinates()->unlock();

    M->assemble();
    K->assemble();

    // /////////////////////////////////////////////////////////////////
    // // Linear Form b corresponds to the rhs
    // /////////////////////////////////////////////////////////////////

    dtkVector<double> *vec = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);
    dtkVector<double> *bsolv = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);
    QHash<QString, dtkVector<double>* > rhs;

    // dtkDistributedArray<double> *vec = new dtkDistributedArray<double>(fes.nbNodes(),0.0);
    // dtkDistributedArray<double> *bsolv = new dtkDistributedArray<double>(fes.nbNodes(),0.0);
    if (vec == nullptr) {
        dtkFatal() << Q_FUNC_INFO << " The dtkVector could not be created by the factory.";
    } else {
        vec->resize(fes.nbNodes());
        bsolv->resize(fes.nbNodes());
        rhs["P1"] = vec;
    }

    for (std::size_t plan = 0; plan < n_plans; ++plan) {
        auto elt_it  = fes.beginFE();
        auto elt_end = fes.endFE();
        dtkFemMatrix localMatrix;
        localMatrix.resize(3,3);
        localMatrix = 0.0;
        dtkFemMassTerm mass;

        for (auto *vec : rhs) {
            vec->fill(0.0);
        }
        for(int i=0; i<fes.nbNodes(); ++i){
            bsolv->setAt(i,1.0);
        }
        // auto& vec_it  = *vec;
        bsolv->rlock();
        for (; elt_it != elt_end; ++elt_it) {

            mass.computeLocalTerm(*(*elt_it), localMatrix);
            auto vec_it  = rhs.begin();
            // for(int i=0;i<3;++i){
            //     for(int j=0;j<3;++j){
            //         if(i==j){
            //             localMatrix(i,j) = 0.0;
            //         }else{
            //             localMatrix(i,j) = 1.0;
            //         }
            //     }
            // }
            int i = (*elt_it)->vertex_id()[0];
            int j = (*elt_it)->vertex_id()[1];
            int k = (*elt_it)->vertex_id()[2];

            // double a = localMatrix(0,0) * bsolv->at(i) + localMatrix(0,1) * bsolv->at(j) + localMatrix(0,2) * bsolv->at(k);
            // double b = localMatrix(1,0) * bsolv->at(i) + localMatrix(1,1) * bsolv->at(j) + localMatrix(1,2) * bsolv->at(k);
            // double c = localMatrix(2,0) * bsolv->at(i) + localMatrix(2,1) * bsolv->at(j) + localMatrix(2,2) * bsolv->at(k);

            // vec_it.addAssign(i,a);
            // vec_it.addAssign(j,b);
            // vec_it.addAssign(k,c);

            if(i==152 || j==152 || k==152){
                dtkTrace()<<"elt_id"<< (*elt_it)->elmt_id();
            }
            // vec_it[i] += localMatrix(0,0) * bsolv->at(i) + localMatrix(0,1) * bsolv->at(j) + localMatrix(0,2) * bsolv->at(k);
            // vec_it[j] += localMatrix(1,0) * bsolv->at(i) + localMatrix(1,1) * bsolv->at(j) + localMatrix(1,2) * bsolv->at(k);
            // vec_it[k] += localMatrix(2,0) * bsolv->at(i) + localMatrix(2,1) * bsolv->at(j) + localMatrix(2,2) * bsolv->at(k);


            (*(*vec_it))[i] += localMatrix(0,0) * bsolv->at(i) + localMatrix(0,1) * bsolv->at(j) + localMatrix(0,2) * bsolv->at(k);

            (*(*vec_it))[j] += localMatrix(1,0) * bsolv->at(i) + localMatrix(1,1) * bsolv->at(j) + localMatrix(1,2) * bsolv->at(k);

            (*(*vec_it))[k] += localMatrix(2,0) * bsolv->at(i) + localMatrix(2,1) * bsolv->at(j) + localMatrix(2,2) * bsolv->at(k);
        }
        bsolv->unlock();
        comm->barrier();
        vec->rlock();
        if(comm->wid() == 0){
            for(int i=0; i<Nbnodes; ++i){
                std::cout<<"vec "<< i << " "<<plan<<" "<<vec->at(i)<<std::endl;
            }
        }
        vec->unlock();
        comm->barrier();

        *MK = *M;
        *MK *= plan*plan;
        *MK += *K;

        comm->barrier();
        // if(comm->wid() == 0){
        //     for(int i=0; i<Nbnodes; ++i){
        //         for(int j=0; j<Nbnodes; ++j){
        //             std::cout<<MK->at(i,j)<<" ";
        //         }
        //         std::cout<<std::endl;
        //     }
        // }
        // comm->barrier();

    }


    // // std::size_t topo_dim = pmesh.topologicalDimension();
    // const auto& connect_test = pmesh.connectivity(0_sz, topo_dim);
    // //const auto& connect_test = pmesh.connectivity(topo_dim, 0_sz);
    // auto it  = connect_test.begin();
    // auto end = connect_test.end();
    // for(; it != end; ++it){
    //         auto vit  = it.begin();
    //         auto vend = it.end();
    //         auto size = it.connectionCount();
    //         if(size != vec->at(it.id())){
    //             //dtkTrace()<<"inf error"<<it.id()<<vec->at(it.id())<<size;
    //         }
    // }



  }

};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
  dtkDistributedApplication *app = dtkDistributed::create(argc, argv);
  app->setApplicationName("poisson  prototype");
  app->setApplicationVersion("0.0.1");

  QCommandLineParser *parser = app->parser();

  QCommandLineOption option_Nplanes("Nplanes", "Number of planes", "Nplanes_value");
  parser->addOption(option_Nplanes);

  QCommandLineOption option_mesh("mesh", "Mesh file", "mesh_value");
  parser->addOption(option_mesh);

  QCommandLineOption option_distributedMesh("distributedMesh", "distributed Mesh file", "distributedMesh_value");
  parser->addOption(option_distributedMesh);

  QCommandLineOption option_solutionFile("solutionFile", "Solution file hdf5", "solutionfile_value");
  parser->addOption(option_solutionFile);

  QCommandLineOption partitionOption("partitions", "number of partitions", "integer", "1");
  parser->addOption(partitionOption);

  QCommandLineOption partitionerImplOption("partitioner", "partitioner implementation. ", "metis|scotch|...", "metis");
  parser->addOption(partitionerImplOption);

  QCommandLineOption showPartitionersOption("show-partitioners", "show partitioner implementations available.");
  parser->addOption(showPartitionersOption);

  app->initialize();

  // ------------ initialize layers

  dtkFemCore::initialize();
  QCommandLineOption verboseOption("verbose", QCoreApplication::translate("main", "verbose plugin initialization"));
  if (parser->isSet(verboseOption)) {
    dtkFemCore::setVerboseLoading(true);
  }

  QStringList partitioners = dtkDiscreteGeometryCore::meshPartitioner::pluginFactory().keys();
  QString partitionersStr = partitioners.filter(QRegExp("^(?!.*Generic).*$")).join(" ");

  if (parser->isSet(showPartitionersOption)) {
    qDebug() << " dynamically loaded partitioners:" << partitionersStr;
    exit(0);
  }
  dtkInfo() << "Dynamicaly loaded partitioner plugins:" << partitionersStr;

  // ------------ check parameters

  if (!parser->isSet(option_mesh) || !parser->isSet(partitionOption)) {
    qCritical() << "Error: no mesh or number of partition were set ! Use --mesh <filename> --partition <integer>" ;
    return 1;
  }

  qWarning()<< parser->value(option_Nplanes)<< parser->value(option_mesh);

  PoissonRunnable PoissonRun;

  // ----- Sets the partitioner

  if (parser->isSet(partitionerImplOption)) {
    if (parser->value(partitionerImplOption) == "metis") {
      PoissonRun.partitioner_impl = "dtkMeshPartitionerMetis";
    } else {
      qWarning() << "Unknown partitioner " << parser->value(partitionerImplOption) << "abort";
      return 1;
    }
  } else {
    PoissonRun.partitioner_impl = "dtkMeshPartitionerMetis";
  }

  if (!partitionersStr.contains(PoissonRun.partitioner_impl)) {
    qWarning() << "Partitioner " << PoissonRun.partitioner_impl << "is not available. Please check your plugin path. Abort.";
    return 1;
  }

  PoissonRun.n_plans = parser->value(option_Nplanes).toInt();
  PoissonRun.meshFile  = parser->value(option_mesh);
  PoissonRun.distributedMesh  = parser->value(option_distributedMesh);
  PoissonRun.solutionFile  = parser->value(option_solutionFile);

  PoissonRun.partition_count = parser->value(partitionOption).toInt();

  app->spawn();
  app->exec(&PoissonRun);
  app->unspawn();

  return 0 ;

}

//
// main.cpp ends here

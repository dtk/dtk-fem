## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

project(dtkFemDebug)

## #################################################################
## Sources
## #################################################################

set(${PROJECT_NAME}_HEADERS)

set(${PROJECT_NAME}_SOURCES
  main.cpp)

## #################################################################
## Build rules
## #################################################################

if(NOT MSVC)
  add_definitions(-Wno-write-strings)
endif(NOT MSVC)

add_executable(${PROJECT_NAME} MACOSX_BUNDLE WIN32
  ${${PROJECT_NAME}_SOURCES}
  ${${PROJECT_NAME}_HEADERS})

## ###################################################################
## Link rules
## ###################################################################

target_link_libraries(${PROJECT_NAME}  Qt5::Core)

target_link_libraries(${PROJECT_NAME}  dtkDistributed)
target_link_libraries(${PROJECT_NAME}  dtkFemCore)
target_link_libraries(${PROJECT_NAME}  dtkLinearAlgebraSparseCore)
target_link_libraries(${PROJECT_NAME}  dtkDiscreteGeometryCore)
target_link_libraries(${PROJECT_NAME}  dtkDiscreteGeometryDistributed)
target_link_libraries(${PROJECT_NAME}  dtkLog)
target_link_libraries(${PROJECT_NAME}  dtkIo)
target_link_libraries(${PROJECT_NAME}  dtkFftw)

######################################################################
### CMakeLists.txt ends here

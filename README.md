# dtk-fem

Finite element layer

# Getting the source code

Obtaining the source code of dtk-fem:

    git clone git@github.com:d-tk/dtk-fem.git
    cd dtk-fem

First, switch to the devel branch:

    git checkout develop

# Before Installation

First, you need to install some libraries

Using Spack:

  Go to the folder where you want to install Spack

    Type the following commands:

    * git clone https://github.com/d-tk/spack.git
    * cd spack
    * git checkout dtk
    * cd bin
    * ./spack install libelf

  In your **~/.bashrc**, add the following lines:

     export SPACK_ROOT=absolute_path_to_spack_dir
     export ARCH="`ls $SPACK_ROOT/share/spack/modules/`"
     export MODULEPATH=$SPACK_ROOT/share/spack/modules/$ARCH
     export PATH=$PATH:$SPACK_ROOT/bin

  Execute your **bashrc** with:

     source ~/.bashrc

* Install mpi or openmpi, for example:

        spack install mpich
        module avail
        module load mpich

* Install FFTW:

        spack install fftw +mpi
        module avail
        module load fftw

* Install METIS:

        http://glaros.dtc.umn.edu/gkhome/metis/metis/download
        Edit the file include/metis.h and specify the width (32 or 64 bits) of the  elementary data type used in METIS. This is controled by the IDXTYPEWIDTH constant

* Install Hypre:

        spack install hypre
        module avail
        module load hypre

* Install Hdf5:

        spack install hdf5
        module avail
        module load hdf5

* Install VTK

* Install BLAZE

* Install dtk

* Install dtk-discrete-geometry

* Install dtk-plugins-discrete-geometry

* Install dtk-io

* Install dtk-plugins-io

* Install dtk-linear-algebra-sparse

* Install dtk-plugins-linear-algebra-sparse

* Install dtk-plugins-distributed

* In your ~/.config

        mkdir inria
        cd inria
        touch dtk-discrete-geometry-core.ini and add to file
            [discrete-geometry-core]
            plugins=path-to-dtk-plugins-discrete-geometry/build/lib64
        touch dtk-distributed.ini and add to file
            [communicator]
            plugins=path-to-dtk-plugins-distributed/build/plugins
        touch dtk-linear-algebra-sparse.ini and add to file
            [linear-algebra-sparse]
            plugins=path-to-dtk-plugins-linear-algebra-sparse/build/lib64
        touch dtk-io.ini and add to file
            [io]
            plugins=path-to-dtk-plugins-io/build/plugins

# Compiling and running

To compile type the following command:

    cd dtk-fem
    mkdir build
    cd build
    cmake ..
    make

To run a dtkFemApp, type the following command:

    mpirun -np 4 ./bin/dtkFemApp --no-spawn --Nplanes 128 --mesh ../data/gridsize20.vtk --distributedMesh gridsize20distr.vtk --solutionFile solgridsize20.h5 --partitions 4 --logfile console --loglevel trace  --policy mpi3

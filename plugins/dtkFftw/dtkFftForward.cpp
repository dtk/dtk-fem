// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkFftForward.h"
#include <iostream>
#include <fftw3.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

fftForward::fftForward(int n_planes, int size_2D):m_nplanes(n_planes), m_size_2D(size_2D)
{

}

fftForward::~fftForward(void)
{

}
void fftForward::setSize(int n_planes, int size_2D){
    m_nplanes = n_planes;
    m_size_2D = size_2D;

}
void fftForward::setData(std::vector<double>& input, std::vector<double>& output){
    m_input_data = input.data();
    m_output_data = output.data();

}
void fftForward::run(){
    int n_planes[] = {m_nplanes};
    fftw_plan PLAN_FOR;
    int rank=1;
    int stride=m_size_2D ;
    int dist=1 ;
    fftw_r2r_kind kind[1]={FFTW_R2HC};



    PLAN_FOR=fftw_plan_many_r2r(rank, n_planes, m_size_2D, m_input_data, n_planes, stride, dist, m_output_data, n_planes, stride, dist, kind, FFTW_ESTIMATE);


    fftw_execute(PLAN_FOR);

    fftw_destroy_plan(PLAN_FOR);

}

//
// dtkFftForward.cpp ends here

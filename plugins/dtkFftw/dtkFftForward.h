// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
 * \file fftForward.h
 * \brief run fft forward using FFTW
 * \author IAAGOUBI Ayoub
 */

#pragma once

#include "dtkFftwExport.h"

#include <iostream>
#include <vector>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

/*! \class fftForward
 * \brief class to define fft forward
 */
class  DTKFFTW_EXPORT fftForward
{
 public:
/*!
  *  \brief Constructor
  *
  *  Constructor of class fftForward
  */
    fftForward(void);

 /*!
  *  \brief Constructor
  *
  *  Constructor of class fftForward
  *
  *  \param n_planes : number of plans
  *  \param size_2D : number of points per plan
  */
    fftForward(int n_planes, int size_2D);

  /*!
  *  \brief destructor
  *
  *  Destructor of classx fftForward
  */
    ~fftForward(void);

 public:
    void setSize(int n_planes, int size_2D);
    void setData(std::vector<double>& input, std::vector<double>& output);
    void run();

 private:
    int m_nplanes;
    int m_size_2D;
    double *m_input_data;
    double *m_output_data;
};

//
// dtkFftForward.h ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:
/*!
 * \file fftBackward.h
 * \brief run fft Backward using FFTW
 * \author IAAGOUBI Ayoub
 */

#pragma once

#include "dtkFftwExport.h"

#include <iostream>
#include <vector>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

/*! \class fftBackward
 * \brief class to define fft Backward
 */
class DTKFFTW_EXPORT fftBackward
{
public:
/*!
  *  \brief Constructor
  *
  *  Constructor of class fftBackward
  */
     fftBackward(void);

 /*!
  *  \brief Constructor
  *
  *  Constructor of class fftBackward
  *
  *  \param n_planes : number of plans
  *  \param size_2D : number of points per plan
  */
     fftBackward(int n_planes, int size_2D);

 /*!
  *  \brief destructor
  *
  *  Destructor of classx fftBackward
  */
    ~fftBackward(void);

public:
    void setSize(int n_planes,int size_2D);
    void setData(std::vector<double>& input, std::vector<double>& output);
    void run();

 private:
    int m_nplanes;
    int m_size_2D;
    double *m_input_data;
    double *m_output_data;

};

//
// dtkFftBackward.h ends here

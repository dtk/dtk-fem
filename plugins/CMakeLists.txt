## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

add_subdirectory(dtkFftw)

## #################################################################
## Targets
## #################################################################

set(DTKFFTW_TARGETS)

set(DTKFFTW_TARGETS ${DTKFFTW_TARGETS} dtkFftw)

## #################################################################
## Target export
## #################################################################

export(TARGETS ${DTKFFTW_TARGETS} FILE "${CMAKE_BINARY_DIR}/dtkFftDepends.cmake")
#### CMakeLists.txt ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkFemTest.h"
#include <dtkDistributed>
#include <dtkMesh.h>
#include <dtkMeshPartitionMap.h>
#include <dtkMeshData>
#include <dtkMeshReaderGeneric>
#include <dtkDiscreteGeometryCore>

#include <dtkDistributedMesh>
#include <dtkFemCore>

#include <QtCore>
#include <QTest>
#include <dtkLog>
#include <vector>

// ///////////////////////////////////////////////////////////////////
// dtkFemTestCase inplementation
// ///////////////////////////////////////////////////////////////////

class dtkFemTestCasePrivate
{
public:
    std::size_t dof_number;
    std::vector<dtkFemIntegrationPoint*> ps;
    dtkMesh *mesh;
    dtkMeshPartitionMap *map;

    double lambda00 = 0.0;
    double lambda01 = 0.0;
    double lambda02 = 0.0;

    double lambda10 = 0.0;
    double lambda11 = 0.0;
    double lambda12 = 0.0;

    double lambda20 = 0.0;
    double lambda21 = 0.0;
    double lambda22 = 0.0;


};

dtkFemTestCase::dtkFemTestCase(void) : d(new dtkFemTestCasePrivate)
{
    d->map = new dtkMeshPartitionMap;
    d->mesh = nullptr;
    d->dof_number = 3UL;
    d->ps.resize(3);

    d->ps[0] = new dtkFemIntegrationPoint(0.5, 0.0, 0.0, 1./3);
    d->ps[1] = new dtkFemIntegrationPoint(0.0, 0.5, 0.0, 1./3);
    d->ps[2] = new dtkFemIntegrationPoint(0.5, 0.5, 0.0, 1./3);

    // d->ps[0] = new dtkFemIntegrationPoint(1./6, 2./3, 0.0, 1./3);
    // d->ps[1] = new dtkFemIntegrationPoint(2./3, 1./6, 0.0, 1./3);
    // d->ps[2] = new dtkFemIntegrationPoint(1./6, 1./6, 0.0, 1./3);

    // d->ps[0] = new dtkFemIntegrationPoint(1./3, 1./3, 0.0, -27./48);
    // d->ps[1] = new dtkFemIntegrationPoint(1./5, 3./5, 0.0, 25./48);
    // d->ps[2] = new dtkFemIntegrationPoint(1./5, 1./5, 0.0, 25./48);
    // d->ps[3] = new dtkFemIntegrationPoint(3./5, 1./5, 0.0, 25./48);

    d->lambda00 = 1 - (d->ps[0])->x() - (d->ps[0])->y();
    d->lambda01 = (d->ps[0])->x();
    d->lambda02 = (d->ps[0])->y();

    d->lambda10 = 1 - (d->ps[1])->x() - (d->ps[1])->y();
    d->lambda11 = (d->ps[1])->x();
    d->lambda12 = (d->ps[1])->y();

    d->lambda20 = 1 - (d->ps[2])->x() - (d->ps[2])->y();
    d->lambda21 = (d->ps[2])->x();
    d->lambda22 = (d->ps[2])->y();
}

dtkFemTestCase::~dtkFemTestCase(void)
{
    delete d->map;
    if (d->mesh) {
        delete d->mesh;
    }
    delete d;
    d = nullptr;

}

void dtkFemTestCase::initTestCase(void)
{
    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");
    dtkDiscreteGeometryCore::initialize();

    dtkMeshReaderGeneric reader;
    QString mesh_file = QFINDTESTDATA("simple_mesh.vtk");
    reader.setMeshFile(mesh_file);
    reader.run();
    d->mesh = reader.mesh();

    QVERIFY(d->mesh != nullptr);

    QString part_file = QFINDTESTDATA("partition.txt");
    d->map->read(part_file);

    //dtkDistributed::policy()->setType("qthread");

    // for (int i = 0; i < 3; ++i) {
    //     dtkDistributed::policy()->addHost("localhost");
    // }

    //dtkDistributed::spawn();
}

void dtkFemTestCase::testBasisFunctions(void)
{

    dtkFemFiniteElementSpace fes;

    dtkFemFiniteElementP1Triangle e( d->ps);

    const dtkFemVector& bfv0 =  e.basisFunctionValues(0);
    const dtkFemVector& bfv1 =  e.basisFunctionValues(1);
    const dtkFemVector& bfv2 =  e.basisFunctionValues(2);

    QCOMPARE(std::size_t(e.dofNumber()), d->dof_number);

    QCOMPARE(bfv0[0], d->lambda00);
    QCOMPARE(bfv0[1], d->lambda01);
    QCOMPARE(bfv0[2], d->lambda02);

    QCOMPARE(bfv1[0], d->lambda10);
    QCOMPARE(bfv1[1], d->lambda11);
    QCOMPARE(bfv1[2], d->lambda12);

    QCOMPARE(bfv2[0], d->lambda20);
    QCOMPARE(bfv2[1], d->lambda21);
    QCOMPARE(bfv2[2], d->lambda22);
}

void dtkFemTestCase::testLocalMatrix(void)
{
    dtkFemFiniteElementSpace fes;
    dtkFemFiniteElementP1Triangle e(d->ps);

    dtkFemMassTerm mass;
    dtkFemMatrix mat;

    // mass.computeLocalTerm(e,mat);
    // std::cout<<"------------ Mass Matrix on integration point -------------- "<<std::endl;
    // for (std::size_t i = 0; i< mat.rows(); ++i){
    //     for (std::size_t j = 0; j< mat.columns(); ++j){
    //         std::cout<<mat(i,j)<<" ";
    //     }
    //     std::cout<<" "<<std::endl;
    // }

    // QCOMPARE(mat(0, 0), 1./6);
    // QCOMPARE(mat(0, 1), 1./12);
    // QCOMPARE(mat(0, 2), 1./12);

    // QCOMPARE(mat(1, 0), 1./12);
    // QCOMPARE(mat(1, 1), 1./6);
    // QCOMPARE(mat(1, 2), 1./12);

    // QCOMPARE(mat(2, 0), 1./12);
    // QCOMPARE(mat(2, 1), 1./12);
    // QCOMPARE(mat(2, 2), 1./6);

}

void dtkFemTestCase::cleanupTestCase(void)
{

}

void dtkFemTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkFemTest, dtkFemTestCase);

//
// dtkFemTest.cpp ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkTest>

// ///////////////////////////////////////////////////////////////////
// dtkFemTestCase interface
// ///////////////////////////////////////////////////////////////////

class dtkFemTestCase : public QObject
{
     Q_OBJECT

public:
    dtkFemTestCase(void);
   ~dtkFemTestCase(void);

private slots:
    void initTestCase(void);

private slots:
    void testBasisFunctions(void);
    void testLocalMatrix(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class dtkFemTestCasePrivate *d;
};



//
// dtkFemTest.h ends here

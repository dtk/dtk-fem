// Version: $Id:  $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>

// ///////////////////////////////////////////////////////////////////
// Layer methods declarations
// ///////////////////////////////////////////////////////////////////

namespace dtkFemCore {

    DTKFEMCORE_EXPORT void initialize();
    DTKFEMCORE_EXPORT void setVerboseLoading(bool b);

}

//
// dtkFemCore.h ends here

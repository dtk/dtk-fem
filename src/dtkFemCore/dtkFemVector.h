// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <blaze/Math.h>
#include <blaze/math/DynamicVector.h>

// ///////////////////////////////////////////////////////////////////
// dtkFemVector is an alias for blaze column dynamic double vector
// ///////////////////////////////////////////////////////////////////

using blaze::DynamicVector;

typedef DynamicVector<double,  blaze::columnVector> dtkFemVector;

typedef blaze::Subvector<dtkFemVector>         dtkFemVectorView;
typedef blaze::Subvector<const dtkFemVector>         dtkFemVectorViewConst;

//
// dtkFemVector.h ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>

#include <dtkDistributedMesh.h>

#include <iterator>

class dtkFemFiniteElement;
class dtkFemIntegrationPoint;

// ///////////////////////////////////////////////////////////////////
// dtkFemFiniteElementSpace interface
// ///////////////////////////////////////////////////////////////////

class DTKFEMCORE_EXPORT dtkFemFiniteElementSpace
{
public:
     dtkFemFiniteElementSpace(void);
     dtkFemFiniteElementSpace(dtkDistributedMesh& mesh, const std::vector<dtkFemIntegrationPoint *>& ps, const QString& label);
    ~dtkFemFiniteElementSpace(void);

public:
    std::vector<dtkFemFiniteElement *>::const_iterator beginFE(void) const { return m_vec.begin(); }
    std::vector<dtkFemFiniteElement *>::const_iterator   endFE(void) const { return m_vec.end(); }

    dtkDistributedArray<double> *coordinates(void) const { return m_coords; }
    dtkDistributedMesh *mesh(void) const  { return m_mesh; }
    dtkDistributedMapper *vtc_mapper(void) const { return m_vtc_mapper; }
    dtkDistributedMapper *ctv_mapper(void) const { return m_ctv_mapper; }

    std::size_t nbNodes(void) const { return m_nbNodes; }
    std::size_t  nbElts(void) const { return m_nbElts; }

private:
    void initialize(void);

private:
    QString m_label;
    std::size_t m_nbNodes;
    std::size_t m_nbElts;
    std::vector<dtkFemFiniteElement *> m_vec;

    dtkDistributedMeshConnectivity m_elt_to_dof;
    dtkDistributedArray<double> *m_coords;
    dtkDistributedMesh* m_mesh;
    dtkDistributedMapper *m_vtc_mapper;
    dtkDistributedMapper *m_ctv_mapper;

    const std::vector<dtkFemIntegrationPoint *> *m_ps;
};

//
// dtkFemFiniteElementSpace.h ends heree

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkFemFiniteElementCTTriangle.h"
#include "dtkFemIntegrationPoint.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////


dtkFemFiniteElementCTTriangle::dtkFemFiniteElementCTTriangle(void)
{
     m_dof_number = 9;
    m_basis_function_values.resize(m_ipoints.size());
    for(dtkVector<double>& bfv : m_basis_function_values) {
        bfv.resize(m_dof_number);
    }
}

dtkFemFiniteElementCTTriangle::~dtkFemFiniteElementCTTriangle()
{

}


const dtkVector<double>& dtkFemFiniteElementCTTriangle::basisFunctionValues(const dtkFemIntegrationPoint& pi)
{
    // dtkVector<double>& bfv = m_basis_function_values[];
}



//
// dtkFemFiniteElementCTTriangle.cpp ends here

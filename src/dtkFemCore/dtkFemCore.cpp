//
//

// Change Log:
//
//

// Code:

/*!
 * \file dtkFemCore.cpp
 * \brief namespace to initialize dtk plugins
 * \author IAAGOUBI Ayoub
 */

#include "dtkFemCore.h"

#include <dtkDiscreteGeometryCore>
#include <dtkIo>
#include <dtkLinearAlgebraSparse>

#include <QtCore>

// /////////////////////////////////////////////////////////////////
// Layer methods implementations
// /////////////////////////////////////////////////////////////////

/*! \namespace dtkFemCore
 *
 * namespace to initialize dtk plugins:
 *
 * Initialize dtkDiscretGeometry
 *
 * Initialize dtkIo
 *
 *Initialize dtkLinearAlgebraSparse
 */
namespace dtkFemCore {

    void initialize()
    {

        dtkDiscreteGeometryCoreSettings geo_settings;
        geo_settings.beginGroup("discrete-geometry-core");
        dtkDiscreteGeometryCore::initialize( geo_settings.value("plugins").toString());
        geo_settings.endGroup();

        dtkIoSettings settings;
        settings.beginGroup("io");
        dtkIo::dataModel::initialize(settings.value("plugins").toString());
        settings.endGroup();

        dtkLinearAlgebraSparseSettings linear_algebra_sparse_settings;
        linear_algebra_sparse_settings.beginGroup("linear-algebra-sparse");
        dtkLinearAlgebraSparse::pluginManager::initialize(linear_algebra_sparse_settings.value("plugins").toString());
        linear_algebra_sparse_settings.endGroup();
        }


    void setVerboseLoading(bool b)
    {
        dtkDiscreteGeometryCore::setVerboseLoading(b);
        //dtkIo::pluginManager::setVerboseLoading(b);
        dtkLinearAlgebraSparse::pluginManager::setVerboseLoading(b);


    }
}


//
// dtkFemCore.cpp ends here

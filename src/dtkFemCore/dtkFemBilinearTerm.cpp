// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
 * \file dtkFemBilinearTerm.cpp
 * \brief Virtual interface to compute Bilinear terms
 * \author IAAGOUBI Ayoub
 */

#include "dtkFemBilinearTerm.h"

// ///////////////////////////////////////////////////////////////////
// dtkfiniteElementSpace implementation
// ///////////////////////////////////////////////////////////////////

dtkFemBilinearTerm::dtkFemBilinearTerm(void){

}

dtkFemBilinearTerm::~dtkFemBilinearTerm(void){

}


//
// dtkFemBilinearTer.cpp ends here

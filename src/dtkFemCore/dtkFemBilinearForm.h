// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>

#include <QtCore>

#include <dtkLinearAlgebraSparse>

class dtkFemFiniteElementSpace;
class dtkFemBilinearTerm;

// ///////////////////////////////////////////////////////////////////
// dtkBilinearForm interface
// ///////////////////////////////////////////////////////////////////

class DTKFEMCORE_EXPORT dtkFemBilinearForm
{
public:
    dtkFemBilinearForm(const dtkFemFiniteElementSpace& fes);

    void addTerm(dtkFemBilinearTerm& term, const QString& label);

    dtkSparseMatrix<double> *matrix(const QString& label) const;

    void assemble(void);

protected:
    const dtkFemFiniteElementSpace *m_fes;
    QHash<QString, dtkFemBilinearTerm *> m_terms;
    QHash<QString, dtkSparseMatrix<double> *> m_matrices;
};

//
// dtkBilinearForm.h ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
 * \file dtkFemMassTerm.cpp
 * \brief The class dtkFemMassTerm defines the mass term of bilinear form
 * \author IAAGOUBI Ayoub
 */

#include "dtkFemMassTerm.h"
#include "dtkFemFiniteElement.h"
#include "dtkFemVector.h"
#include "dtkFemIntegrationPoint.h"

#include <iostream>

// ///////////////////////////////////////////////////////////////////
// dtkFemMassTerm implementation
// ///////////////////////////////////////////////////////////////////

/*!
  * \brief
  * Constructor of class dtkFemMassTerm
  *
*/
dtkFemMassTerm::dtkFemMassTerm(void) : dtkFemBilinearTerm()
{

}

/*!
  * \brief
  * Destructor of class dtkFemMassTerm
  *
*/
dtkFemMassTerm::~dtkFemMassTerm(void)
{

}

/*!
  * \brief
  * For a given element, return the corresponding mass  matrix.
  *
  *
*/
void dtkFemMassTerm::computeLocalTerm(const dtkFemFiniteElement& fe_elt, dtkFemMatrix& mat) const
{
    auto pi_it  = fe_elt.integrationPoints().begin();
    auto pi_end = fe_elt.integrationPoints().end();

    mat.resize(fe_elt.dofNumber(), fe_elt.dofNumber());
    mat = 0.0;

    // Loop over all points integration
    for (std::size_t i = 0; pi_it != pi_end; ++pi_it, ++i) {
        // Retrieves the basis functions for integration point i
        const dtkFemVector& bf = fe_elt.basisFunctionValues(i);
        // Transposes the basis function vector
        auto transbf = trans(bf);
        // Multiply by the weight of  integration  point and add the contribution to the local mass matrix
        mat += (*pi_it)->w() * bf * transbf;
    }
    // Multiply by the measure of element
    mat *= fe_elt.measure();
}


//
// dtkFemMassTerm.cpp ends here

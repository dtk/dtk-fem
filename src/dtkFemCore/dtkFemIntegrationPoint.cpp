// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
 * \file dtkFemIntegrationPoint.cpp
 * \brief Class to define integration point
 * \author Thibaud Kloczko
 */

#include "dtkFemIntegrationPoint.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkFemIntegrationPoint::dtkFemIntegrationPoint(void) : SuperClass({0., 0., 0., 0.})
{
}

dtkFemIntegrationPoint::dtkFemIntegrationPoint(const double x, const double y, const double z, const double w) : SuperClass({x, y, z, w})
{
}

dtkFemIntegrationPoint::dtkFemIntegrationPoint(const double *coords, const double weight) : SuperClass({coords[0], coords[1], coords[2], weight})
{
}

dtkFemIntegrationPoint::dtkFemIntegrationPoint(const double *array) : SuperClass({array[0], array[1], array[2], array[3]})
{
}

dtkFemIntegrationPoint::dtkFemIntegrationPoint(const std::array<double, 4>& array) : SuperClass(array)
{
}

dtkFemIntegrationPoint& dtkFemIntegrationPoint::operator = (const std::array<double, 4>& array)
{
    auto& self = static_cast<SuperClass&>(*this);
    self = array;

    return *this;
}

dtkFemIntegrationPoint& dtkFemIntegrationPoint::operator = (const dtkFemIntegrationPoint& o)
{
    auto& self = static_cast<SuperClass&>(*this);
    self = static_cast<const SuperClass&>(o);

    return *this;
}

void dtkFemIntegrationPoint::set(const double x, const double y, const double z, const double w)
{
    auto& self = static_cast<SuperClass&>(*this);
    self = {x, y, z, w};
}

void dtkFemIntegrationPoint::set(const double *coords, const double weight)
{
    auto& self = static_cast<SuperClass&>(*this);
    self = {coords[0], coords[1], coords[2], weight};
}

void dtkFemIntegrationPoint::set(const double *array)
{
    auto& self = static_cast<SuperClass&>(*this);
    self = {array[0], array[1], array[2], array[3]};
}

void dtkFemIntegrationPoint::set(const std::array<double, 4>& array)
{
    *this = array;
}

void dtkFemIntegrationPoint::copyCoords(double *coords) const
{
    coords[0] = (*this)[0];
    coords[1] = (*this)[1];
    coords[2] = (*this)[2];
}

void dtkFemIntegrationPoint::copyArray(double *array) const
{
    array[0] = (*this)[0];
    array[1] = (*this)[1];
    array[2] = (*this)[2];
    array[3] = (*this)[3];
}

//
// dtkFemIntegrationPoint.cpp ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>

#include "dtkFemBilinearTerm.h"

#include "dtkFemMatrix.h"

class dtkFemFiniteElement;

// ///////////////////////////////////////////////////////////////////
// dtkMassTerm interface
// ///////////////////////////////////////////////////////////////////

class DTKFEMCORE_EXPORT dtkFemMassTerm : public dtkFemBilinearTerm
{
public:
    dtkFemMassTerm(void);
   ~dtkFemMassTerm(void);

public:
    void computeLocalTerm(const dtkFemFiniteElement& fe_elt, dtkFemMatrix& mat) const;

};

//
// dtkFemMassTerm.h ends here

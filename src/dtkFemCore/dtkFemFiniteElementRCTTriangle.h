// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>

#include "dtkFemFiniteElement.h"
#include "dtkFemVector.h"
#include "dtkFemMatrix.h"

class dtkDistributedMesh;
class dtkFemIntegrationPoint;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKFEMCORE_EXPORT dtkFemFiniteElementRCTTriangle : public dtkFemFiniteElement
{
public:
     dtkFemFiniteElementRCTTriangle(void);
     //dtkFemFiniteElementRCTTriangle(const std::vector<dtkFemIntegrationPoint *>& gauss_pts);
     dtkFemFiniteElementRCTTriangle(std::size_t elt_id, const dtkDistributedMesh& mesh, const std::vector<dtkFemIntegrationPoint *>& gauss_pts);
    ~dtkFemFiniteElementRCTTriangle(void);

public:
    const dtkFemVector& basisFunctionValues(std::size_t pi) const { return m_basis_values[pi]; }
    const dtkFemMatrix& gradBasisFunctionValues(std::size_t pi) const { return m_grad_basis_values[pi]; }
    const double        measure(void) const { return 0; }

private:
    void computeEccentricity(void);
    void computeBasisLocalDof(void);

private:
    std::vector<dtkFemVector> m_basis_values;
    std::vector<dtkFemMatrix> m_grad_basis_values;
    dtkFemMatrix m_dof_loc;
    std::array<double, 3> m_ecc;
};

//
// dtkFemFiniteElementRCTTriangle.h ends here

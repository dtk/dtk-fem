// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>

#include "dtkFemBilinearTerm.h"

#include "dtkFemMatrix.h"

class dtkFemFiniteElement;

// ///////////////////////////////////////////////////////////////////
// dtkStifnessTerm interface
// ///////////////////////////////////////////////////////////////////

class DTKFEMCORE_EXPORT dtkFemDiffusionTerm : public dtkFemBilinearTerm
{
public:
     dtkFemDiffusionTerm(void);
    ~dtkFemDiffusionTerm(void);

public:
    void computeLocalTerm(const dtkFemFiniteElement& fe_elt, dtkFemMatrix& mat) const;
};

//
// dtkStifnessTerm.h ends here

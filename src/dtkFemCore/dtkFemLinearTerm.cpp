// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
 * \file dtkFemLinearTerm.cpp
 * \brief Virtual interface to compute liear term
 * \author IAAGOUBI Ayoub
 */
#include "dtkFemLinearTerm.h"
#include "dtkFemFiniteElement.h"

// ///////////////////////////////////////////////////////////////////
// dtkFemLinearTerm implementation
// ///////////////////////////////////////////////////////////////////

/*!
  * \brief
  * Constructor of class dtkFemLinearTerm
  *
*/
dtkFemLinearTerm::dtkFemLinearTerm(void){

}

/*!
  * \brief
  * Destructor of class dtkFemLinearTerm
  *
*/
dtkFemLinearTerm::~dtkFemLinearTerm(void){

}

/*! \fn virtual dtkVector<double>* dtkFemLinearTerm::computeLinearTerm()
  return a dtkVecor of linear term
*/

//
// dtkFemLinearTerm.cpp ends here

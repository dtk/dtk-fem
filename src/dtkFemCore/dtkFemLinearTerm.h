// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>
#include <dtkLinearAlgebraSparse>

class dtkFemFiniteElement;

// ///////////////////////////////////////////////////////////////////
// dtkFemFiniteLinearSpace interface
// ///////////////////////////////////////////////////////////////////

class DTKFEMCORE_EXPORT dtkFemLinearTerm
{
 public:
    dtkFemLinearTerm(void);
    virtual ~dtkFemLinearTerm(void);

 public:

    virtual void computeLinearTerm(const dtkFemFiniteElement& fe_elt, std::vector<double>& v ) const = 0;
 protected:
    dtkVector<double>* m_b;
};

//
// dtkFemFiniteLinearTerm.h ends here

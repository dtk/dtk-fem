// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
 * \file dtkFemBilinearForm.cpp
 * \brief The class dtkFemBilinearForm defines the bilinear form of a problem
 *  by calculating all the terms of mass, diffusion ... and assemble all the elements.
 * \author IAAGOUBI Ayoub
 */

#include "dtkFemBilinearForm.h"

#include "dtkFemFiniteElement.h"
#include "dtkFemMatrix.h"
#include "dtkFemFiniteElementSpace.h"
#include "dtkFemBilinearTerm.h"

// ///////////////////////////////////////////////////////////////////
// dtkBilinearForm implementation
// ///////////////////////////////////////////////////////////////////

/*!
  * \brief
  * Constructor of class dtkFemBilinearForm
  *
*/

dtkFemBilinearForm::dtkFemBilinearForm(const dtkFemFiniteElementSpace& fes) : m_fes(&fes)
{

}

/*!
  * \brief
  * A term is defined by a label, for example if we want add a local mass term, it is enough to write:
  *
  * dtkFemBilinearTerm mass;
  *
  * addTerm(mass, "u");
  *
  * To initialize the global matrix, it is necessary to define a graph with a mapper vertex to cell then set the graph for the sparse matrix.
  *
*/

void dtkFemBilinearForm::addTerm( dtkFemBilinearTerm& term, const QString& label)
{
    QString matrixImpl = "dtkDistributedSparseMatrixEngineCSR";
    dtkSparseMatrix< double > *sparse_matrix = dtkLinearAlgebraSparse::matrixFactory::create<double>(matrixImpl);
    dtkDistributedGraphTopology *matrix_graph = new dtkDistributedGraphTopology(m_fes->nbNodes(),m_fes->vtc_mapper());

    if (sparse_matrix == nullptr) {
        dtkFatal() << Q_FUNC_INFO << " The dtkSparseMatrix could not be created by the factory.";
    } else {

        sparse_matrix->setGraph(matrix_graph);
        m_terms[label] = &term;
        m_matrices[label] = sparse_matrix;
    }

}

/*!
  * \brief
  * This method allows to recover de matrix (Mass or Diffusion) using the same label defined in addTerm.
  *
*/

dtkSparseMatrix<double> *dtkFemBilinearForm::matrix(const QString& label) const
{
    return m_matrices[label];
}

/*!
  * \brief
  * To assemble, we loop on the local elements that correspond to each partition.
  *
  * For each element and for each term, we compute the local matrix and then add the contributions to construct a global matrix.
  *
  * Here we assemble using  a local matrix of size (3,3), it must be adapted to compute a local matrix with size(dofNumber, dofNumber). For Clough-tocher, for example, we use a local matrix (9,9).
*/
void dtkFemBilinearForm::assemble(void)
{
    if (m_terms.empty()) {
        return;
    }

    dtkFemMatrix localMatrix;
    auto elt_it  = m_fes->beginFE();
    auto elt_end = m_fes->endFE();

    for (; elt_it != elt_end; ++elt_it) {

        auto term_it  = m_terms.begin();
        auto term_end = m_terms.end();

        auto mat_it  = m_matrices.begin();

        for (; term_it != term_end; ++term_it, ++mat_it ) {

            (*term_it)->computeLocalTerm(*(*elt_it), localMatrix);

            // i,j,k are the 3 vertex id for each element.
            long i = (*elt_it)->vertex_id()[0];
            long j = (*elt_it)->vertex_id()[1];
            long k = (*elt_it)->vertex_id()[2];

            (*mat_it)->addAssign(i, i, localMatrix(0, 0));
            (*mat_it)->addAssign(i, j, localMatrix(0, 1));
            (*mat_it)->addAssign(i, k, localMatrix(0, 2));

            (*mat_it)->addAssign(j, i, localMatrix(1, 0));
            (*mat_it)->addAssign(j, j, localMatrix(1, 1));
            (*mat_it)->addAssign(j, k, localMatrix(1, 2));

            (*mat_it)->addAssign(k, i, localMatrix(2, 0));
            (*mat_it)->addAssign(k, j, localMatrix(2, 1));
            (*mat_it)->addAssign(k, k, localMatrix(2, 2));

        }

    }

}

//
// dtkFemBilinearForm.cpp ends here

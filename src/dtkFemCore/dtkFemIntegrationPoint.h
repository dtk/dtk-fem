// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>

#include <array>

// ///////////////////////////////////////////////////////////////////
// dtkFemIntegrationPoint interface
// ///////////////////////////////////////////////////////////////////

class DTKFEMCORE_EXPORT dtkFemIntegrationPoint : public std::array<double, 4>
{
    typedef std::array<double, 4> SuperClass;

public:
    dtkFemIntegrationPoint(void);
    dtkFemIntegrationPoint(const double x, const double y, const double z, const double w);
    dtkFemIntegrationPoint(const double *coords, const double weight);
    dtkFemIntegrationPoint(const double *array);
    dtkFemIntegrationPoint(const std::array<double, 4>& array);
    dtkFemIntegrationPoint(const dtkFemIntegrationPoint& o);

public:
    dtkFemIntegrationPoint& operator = (const std::array<double, 4>& o);
    dtkFemIntegrationPoint& operator = (const dtkFemIntegrationPoint& o);

public:
    operator const std::array<double, 4>&() const { return *this; }
    operator       std::array<double, 4>&()       { return *this; }

public:
    const double& x(void) const { return (*this)[0]; }
    const double& y(void) const { return (*this)[1]; }
    const double& z(void) const { return (*this)[2]; }
    const double& w(void) const { return (*this)[3]; }

    double& x(void) { return (*this)[0]; }
    double& y(void) { return (*this)[1]; }
    double& z(void) { return (*this)[2]; }
    double& w(void) { return (*this)[3]; }

public:
    void set(const double x, const double y, const double z, const double w);
    void set(const double *coords, const double weight);
    void set(const double *array);
    void set(const std::array<double, 4>& array);

public:
    void copyCoords(double *coords) const;
    void  copyArray(double *array)  const;
};

//
// dtkFemIntegrationPoint.h ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
 * \file dtkFemDomainIntegratorTerm.cpp
 * \brief Class to de fine domain integrator term
 * \author IAAGOUBI Ayoub
 */

#include "dtkFemDomainIntegratorTerm.h"
#include "dtkFemFiniteElement.h"
#include "dtkFemMassTerm.h"

// ///////////////////////////////////////////////////////////////////
// dtkFemDomaineIntegratorTerm implementation
// ///////////////////////////////////////////////////////////////////

/*!
  * \brief
  * Constructor of class dtkFemDomainIntegratorTerm
  *
*/
dtkFemDomainIntegratorTerm::dtkFemDomainIntegratorTerm(void) : dtkFemLinearTerm()
{

}

/*!
  * \brief
  * Constructor with a dtkVector
  *
*/
dtkFemDomainIntegratorTerm::dtkFemDomainIntegratorTerm( dtkVector<double>* b)
{
    m_b = b;
}

/*!
  * \brief
  * Destructor of class dtkFemDomainIntegratorTerm
  *
*/
dtkFemDomainIntegratorTerm::~dtkFemDomainIntegratorTerm(void)
{

}

/*!
  * \brief
  * return a dtkVector corresponding to linear term
  *
*/


void dtkFemDomainIntegratorTerm::computeLinearTerm(const dtkFemFiniteElement& fe_elt, std::vector<double>& v ) const
{
    dtkFemMatrix localMatrix;
    dtkFemMassTerm mass;
    v.resize((fe_elt).dofNumber());
    mass.computeLocalTerm(fe_elt, localMatrix);

    // i,j,k are the 3 vertex id for each element.
    int i = (fe_elt).vertex_id()[0];
    int j = (fe_elt).vertex_id()[1];
    int k = (fe_elt).vertex_id()[2];

    v[0] = localMatrix(0,0) * m_b->at(i) + localMatrix(0,1) * m_b->at(j) + localMatrix(0,2) * m_b->at(k);
    v[1] = localMatrix(1,0) * m_b->at(i) + localMatrix(1,1) * m_b->at(j) + localMatrix(1,2) * m_b->at(k);
    v[2] = localMatrix(2,0) * m_b->at(i) + localMatrix(2,1) * m_b->at(j) + localMatrix(2,2) * m_b->at(k);


}
//
// dtkFemDomaineIntegratorTerm.cpp ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
 * \file dtkFemFiniteElement.cpp
 * \brief Virtual interface to define a finite element
 * \author IAAGOUBI Ayoub
 */

#include "dtkFemFiniteElement.h"
#include "dtkFemIntegrationPoint.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

/*!
  * \brief
  * Constructor of class dtkFemFiniteElement
  *
*/
dtkFemFiniteElement::dtkFemFiniteElement(void)
{

}
/*!
  * \brief
  * Destructor of class dtkFemFiniteElement
  *
*/
dtkFemFiniteElement::~dtkFemFiniteElement()
{

}

/*! \fn const std::vector<dtkFemIntegrationPoint*> dtkFemFiniteElement::integrationPoints(void)
  return a vector of dtkFemIntegrationPoint
*/

/*! \fn int dtkFemFiniteElement::dofNumber(void)
  return a degree of freedom of finite element
*/

/*! \fn int dtkFemFiniteElement::elmt_id(void)
  return element id
*/

/*! \fn const std::vector<int> dtkFemFiniteElement::vertex_id(void)
  return a vector of vertex id for finite element
*/

/*! \fn  dtkFemMatrix dtkFemFiniteElement::coord(void)
  return coordinates(x,y) of element vertices
*/

/*! \fn virtual const dtkFemVectorViewConst dtkFemFiniteElement::basisFunctionValues(std::size_t pi)
  Compute basis functions vallues for a given integration point
*/

/*! \fn virtual const dtkFemMatrix  dtkFemFiniteElement::gradBasisFunctionValues()
  Compute gradient of basis functions values
*/

/*! \fn virtual const double  dtkFemFiniteElement:: measure(void)
  Compute measure for finite element
*/

//
// dtkFemFiniteElement.cpp ends here

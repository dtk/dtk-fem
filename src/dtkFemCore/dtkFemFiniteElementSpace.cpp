// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
 * \file dtkFemFiniteElementSpace.cpp
 * \brief The class dtkFemFiniteElementSpace defines a finite element space
 *
 * A finite element space is defined by a vector of finite element
 * \author IAAGOUBI Ayoub
 */

#include "dtkFemFiniteElementSpace.h"
#include "dtkFemIntegrationPoint.h"
#include "dtkFemFiniteElementP1Triangle.h"


// ///////////////////////////////////////////////////////////////////
// dtkFemFiniteElementSpace implementation
// ///////////////////////////////////////////////////////////////////

/*!
  * \brief
  * Constructor of class dtkFemFiniteElementSpace
  *
*/
dtkFemFiniteElementSpace::dtkFemFiniteElementSpace(void)
{

}

/*!
  * \brief
  * Constructor with a distributed mesh, vector of integrations points and a label.
  *
  * label = "P1" ----> dtkFemFiniteElementP1Triangle
  *
  * label = "CT" ----> dtkFemFiniteElementCTTriangle
*/
dtkFemFiniteElementSpace::dtkFemFiniteElementSpace(dtkDistributedMesh& mesh, const std::vector<dtkFemIntegrationPoint *>& ps, const QString& label)
{

    m_nbElts = mesh.cellCount();
    m_nbNodes = mesh.vertexCount();
    m_label = label;
    m_coords = mesh.coordinates();
    m_mesh = &mesh;
    m_ps = &ps;

    int topo_dim = mesh.topologicalDimension();
    dtkDistributedGraphTopology *vtc_graph =  mesh.connectivity(0, topo_dim).graph();
    m_vtc_mapper = vtc_graph->mapper();

    dtkDistributedGraphTopology *ctv_graph =  mesh.connectivity(topo_dim, 0).graph();
    m_ctv_mapper = ctv_graph->mapper();

    this->initialize();
}

/*!
  * \brief
  * Destructor of class dtkFemFiniteElementSpace
  *
*/
dtkFemFiniteElementSpace::~dtkFemFiniteElementSpace(void)
{

}

/*!
  * \brief
  * Using a connectivity topodim-->0, we construct a vector of finite element (P1 or CT ...) that contains the local elements for each partition.
  *
*/
void dtkFemFiniteElementSpace::initialize(void)
{
    if (m_label == "P1") {
        m_elt_to_dof = m_mesh->connectivity(m_mesh->topologicalDimension(), 0);
        auto entity_it = m_elt_to_dof.begin();
        auto entity_end = m_elt_to_dof.end();
        std::size_t size = std::distance(entity_it, entity_end);
        m_vec.resize(size);

        auto felt_it = m_vec.begin();
        for(; entity_it != entity_end; ++entity_it, ++felt_it) {
            *felt_it = new dtkFemFiniteElementP1Triangle(entity_it.id(), *m_mesh, *m_ps);
        }

    } else {
        dtkFatal() << Q_FUNC_INFO << "Kind of finite element not yet implemented: " << m_label;
    }
}

//
// dtkFemFiniteElementSpace.cpp ends here

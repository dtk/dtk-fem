// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>

#include "dtkFemFiniteElement.h"

#include "dtkFemVector.h"
#include "dtkFemMatrix.h"

#include <dtkDistributedMesh.h>

#include <vector>

class dtkFemIntegrationPoint;

// ///////////////////////////////////////////////////////////////////
// dtkFemFiniteElementP1Triangle interface
// ///////////////////////////////////////////////////////////////////

class DTKFEMCORE_EXPORT dtkFemFiniteElementP1Triangle : public dtkFemFiniteElement
{
public:
     dtkFemFiniteElementP1Triangle(void);
     dtkFemFiniteElementP1Triangle(const std::vector<dtkFemIntegrationPoint *>& ps);
     dtkFemFiniteElementP1Triangle(std::size_t indexElt, const dtkDistributedMesh& mesh, const std::vector<dtkFemIntegrationPoint *>& ps);
    ~dtkFemFiniteElementP1Triangle(void);

    const dtkFemVector& basisFunctionValues(std::size_t pi) const;
    const dtkFemMatrix& gradBasisFunctionValues(std::size_t pi) const { return m_gradBasisFunctionValues; }
    const double        measure(void) const;

public:
    // const std::vector<dtkFemVector>& basis_function_values(void) const { return  m_basis_function_values; }
    //       std::vector<dtkFemVector>& basis_function_values(void)       { return  m_basis_function_values; }

private:
    void computeGradBasisFunctionValues(void);

protected:
    std::vector<dtkFemVector> m_basis_function_values;
    dtkFemMatrix m_gradBasisFunctionValues;
};

//
// dtkFemFiniteElementP1Triangle.h ends here

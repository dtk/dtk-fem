// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>

#include "dtkFemFiniteElement.h"

#include <dtkLinearAlgebraSparse>

#include <iostream>
#include <string>
#include <vector>

class dtkFemIntegrationPoint;

// ///////////////////////////////////////////////////////////////////
// dtkFemFiniteElement CT Triangle interface
// ///////////////////////////////////////////////////////////////////

class DTKFEMCORE_EXPORT dtkFemFiniteElementCTTriangle : public dtkFemFiniteElement
{
public:
     dtkFemFiniteElementCTTriangle(void);
    ~dtkFemFiniteElementCTTriangle();

    const dtkVector<double>& basisFunctionValues(const dtkFemIntegrationPoint& pi);

protected:

    std::vector<dtkVector<double>> m_basis_function_values;
};


//
// dtkFemFiniteElementCTTriangle.h ends here

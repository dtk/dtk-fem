// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
 * \file dtkFemFiniteElementP1Triangle.cpp
 * \brief class to define a finite element P1
 * \author IAAGOUBI Ayoub
 */

#include "dtkFemFiniteElementP1Triangle.h"

#include "dtkFemIntegrationPoint.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

/*!
  * \brief
  * Constructor of class dtkFemFiniteElementP1Triangle
  *
*/
dtkFemFiniteElementP1Triangle::dtkFemFiniteElementP1Triangle(void)
{

}

/*!
  * \brief
  * Constructor with vector of integrations points
  *
*/
dtkFemFiniteElementP1Triangle::dtkFemFiniteElementP1Triangle(const std::vector<dtkFemIntegrationPoint*>& ps)
{
    m_dof_number = 3;
    m_basis_function_values.resize(ps.size());

    auto bfv = m_basis_function_values.begin();
    for (auto pi = ps.begin(); pi != ps.end(); ++pi, ++bfv) {

        bfv->resize(m_dof_number);

        (*bfv)[1] = (*pi)->x();
        (*bfv)[2] = (*pi)->y();
        (*bfv)[0] = 1 - (*bfv)[1] - (*bfv)[2];
    }
    m_ipoints = ps;
}

/*!
  * \brief
  * Constructor with mesh, element in and vector of integrations points
  *
  * We use a distributed mesh to compute vertex_id and coordinates
  *
  * In P1, the 3 basis functions are the 3 barycentric coordiantes(lambda1, lambda2, lambda3)
  *
  * For each integration point, we compute the 3 corresponding base functions
*/
dtkFemFiniteElementP1Triangle::dtkFemFiniteElementP1Triangle(std::size_t indexElt, const dtkDistributedMesh& mesh, const std::vector<dtkFemIntegrationPoint *>& ps)
{
    m_dof_number = 3;
    m_elmt_id = indexElt;
    m_basis_function_values.resize(ps.size());

    auto bfv = m_basis_function_values.begin();
    for (auto pi = ps.begin(); pi != ps.end(); ++pi, ++bfv) {

        bfv->resize(m_dof_number);

        (*bfv)[1] = (*pi)->x();
        (*bfv)[2] = (*pi)->y();
        (*bfv)[0] = 1 - (*bfv)[1] - (*bfv)[2];
    }
    m_ipoints = ps;

    m_vertex_id.resize(m_dof_number);
    m_vertex_id[0] = mesh.new_connections_new_vid()->at(3 * indexElt    );
    m_vertex_id[1] = mesh.new_connections_new_vid()->at(3 * indexElt + 1);
    m_vertex_id[2] = mesh.new_connections_new_vid()->at(3 * indexElt + 2);

    m_coord.resize(3,2);

    m_coord(0, 0) = mesh.coordinates()->at(3 * m_vertex_id[0]    );
    m_coord(0, 1) = mesh.coordinates()->at(3 * m_vertex_id[0] + 1);

    m_coord(1, 0) = mesh.coordinates()->at(3 * m_vertex_id[1]    );
    m_coord(1, 1) = mesh.coordinates()->at(3 * m_vertex_id[1] + 1);

    m_coord(2, 0) = mesh.coordinates()->at(3 * m_vertex_id[2]    );
    m_coord(2, 1) = mesh.coordinates()->at(3 * m_vertex_id[2] + 1);

    computeGradBasisFunctionValues();
}

/*!
  * \brief
  * Destructor of class dtkFemFiniteElementP1Triangle
  *
*/
dtkFemFiniteElementP1Triangle::~dtkFemFiniteElementP1Triangle()
{

}

/*!
  * \brief
  * return the 3 basis functions values for a given integration point
  *
*/
const dtkFemVector& dtkFemFiniteElementP1Triangle::basisFunctionValues(std::size_t pi) const
{
    return m_basis_function_values[pi];
}

/*!
  * \brief
  * Compute the gradient of basis functions values:
  *
  * m_gradBasisFunctionValues(0,0) = dlambda1/dx , m_gradBasisFunctionValues(0,1) = dlambda1/dy
  *
  * m_gradBasisFunctionValues(1,0) = dlambda2/dx , m_gradBasisFunctionValues(1,1) = dlambda2/dy
  *
  * m_gradBasisFunctionValues(2,0) = dlambda3/dx , m_gradBasisFunctionValues(2,1) = dlambda3/dy
  *
*/
void dtkFemFiniteElementP1Triangle::computeGradBasisFunctionValues(void)
{
    m_gradBasisFunctionValues.resize(3, 2);

    double x1 = m_coord(0, 0);
    double y1 = m_coord(0, 1);

    double x2 = m_coord(1, 0);
    double y2 = m_coord(1, 1);

    double x3 = m_coord(2, 0);
    double y3 = m_coord(2, 1);

    double aire = 0.5 * std::abs((x2-x1) * (y3-y1) - (y2-y1) * (x3-x1));

    m_gradBasisFunctionValues(0, 0) = (y2 - y3) / (2 * aire);
    m_gradBasisFunctionValues(0, 1) = (x3 - x2) / (2 * aire);

    m_gradBasisFunctionValues(1, 0) = (y3 - y1) / (2 * aire);
    m_gradBasisFunctionValues(1, 1) = (x1 - x3) / (2 * aire);

    m_gradBasisFunctionValues(2, 0) = (y1 - y2) / (2 * aire);
    m_gradBasisFunctionValues(2, 1) = (x2 - x1) / (2 * aire);
}

/*!
  * \brief
  * Compute triangle measure
  *
*/
const double dtkFemFiniteElementP1Triangle::measure(void) const
{
    double x1 = m_coord(0, 0);
    double y1 = m_coord(0, 1);

    double x2 = m_coord(1, 0);
    double y2 = m_coord(1, 1);

    double x3 = m_coord(2, 0);
    double y3 = m_coord(2, 1);

    double aire = 0.5 * std::abs((x2-x1)*(y3-y1) - (y2-y1)*(x3-x1));

    return aire;

}
//
// dtkFemFiniteElementP1Triangle.cpp ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
 * \file dtkFemDiffusionTerm.cpp
 * \brief The class dtkFemDiffusionTerm defines the diffusion term of bilinear form
 * \author IAAGOUBI Ayoub
 */

#include "dtkFemDiffusionTerm.h"
#include "dtkFemFiniteElement.h"
#include "dtkFemIntegrationPoint.h"
#include "dtkFemVector.h"

// ///////////////////////////////////////////////////////////////////
// dtkFemDiffusionTerm implementation
// ///////////////////////////////////////////////////////////////////

/*!
  * \brief
  * Constructor of class dtkFemDiffusionTerm
  *
*/
dtkFemDiffusionTerm::dtkFemDiffusionTerm(void) : dtkFemBilinearTerm()
{

}

/*!
  * \brief
  * Destructor of class dtkFemDiffusionTerm
  *
*/
dtkFemDiffusionTerm::~dtkFemDiffusionTerm(void)
{

}

/*!
  * \brief
  * For a given element, return the corresponding stiffness matrix.
  *
  *
*/
void dtkFemDiffusionTerm::computeLocalTerm(const dtkFemFiniteElement& fe_elt, dtkFemMatrix& mat) const
{
    auto pi_it  = fe_elt.integrationPoints().begin();
    auto pi_end = fe_elt.integrationPoints().end();

    mat.resize(fe_elt.dofNumber(), fe_elt.dofNumber());
    mat = 0.0;

    // Loop over all points integration
    for (std::size_t i = 0; pi_it != pi_end; ++pi_it, ++i) {
        // Retrieves the gradients of basis functions for integration point i
        const dtkFemMatrix& Gradbfv = fe_elt.gradBasisFunctionValues(i);
        // Transposes the gradient matrix
        auto tranGradbfv = trans(Gradbfv);
        // Multiply by the weight of integration point and add the contribution to the local stiffness matrix
        mat += (*pi_it)->w() * Gradbfv * tranGradbfv;
    }
    // Multiply by the measure of element
    mat *= fe_elt.measure();
}


//
// dtkFemDiffusionTerm.cpp ends here

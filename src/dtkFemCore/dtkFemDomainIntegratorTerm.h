// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>
#include "dtkFemLinearTerm.h"
#include <vector>

class dtkFemFiniteElement;

// ///////////////////////////////////////////////////////////////////
// dtkFemDomainIntegratorTerm interface
// ///////////////////////////////////////////////////////////////////

class DTKFEMCORE_EXPORT dtkFemDomainIntegratorTerm : public dtkFemLinearTerm
{
public:
    dtkFemDomainIntegratorTerm(void);
    dtkFemDomainIntegratorTerm(dtkVector<double>* b);
   ~dtkFemDomainIntegratorTerm(void);

public:

   void computeLinearTerm(const dtkFemFiniteElement& fe_elt, std::vector<double>& v ) const;
};
//
// dtkFemDomainIntegrator.h ends here

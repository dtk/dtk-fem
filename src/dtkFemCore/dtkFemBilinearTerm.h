// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>

#include "dtkFemMatrix.h"

class dtkFemFiniteElement;

// ///////////////////////////////////////////////////////////////////
// dtkFemBilinearTerm interface
// ///////////////////////////////////////////////////////////////////

class DTKFEMCORE_EXPORT dtkFemBilinearTerm
{
public:
             dtkFemBilinearTerm(void);
    virtual ~dtkFemBilinearTerm(void);

public:
    virtual void computeLocalTerm(const dtkFemFiniteElement& fe_elt, dtkFemMatrix& mat) const = 0;

};

//
// dtkFemBilinearTerm.h ends here

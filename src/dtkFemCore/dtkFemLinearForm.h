// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>

#include "dtkFemFiniteElementSpace.h"

#include <dtkLinearAlgebraSparse>

#include <QtCore>

class dtkFemLinearTerm;

// ///////////////////////////////////////////////////////////////////
// dtkLinearForm interface
// ///////////////////////////////////////////////////////////////////

class DTKFEMCORE_EXPORT dtkFemLinearForm
{
 public:
    dtkFemLinearForm(const dtkFemFiniteElementSpace& fes);
    ~dtkFemLinearForm(void);

 public:
    void addTerm(dtkFemLinearTerm *term, const QString& label);

    dtkVector<double> *rhs(const QString& label) const;

    void assemble();
 protected:
    dtkFemFiniteElementSpace m_fes;
    QHash<QString, dtkFemLinearTerm*> m_terms;
    QHash<QString, dtkVector<double>* > m_rhs;
};

//
// dtkFemLinearForm.h ends here

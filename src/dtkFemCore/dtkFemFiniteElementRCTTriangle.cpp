// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkFemFiniteElementRCTTriangle.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////


dtkFemFiniteElementRCTTriangle::dtkFemFiniteElementRCTTriangle(void) : dtkFemFiniteElement()
{

}

//dtkFemFiniteElementRCTTriangle::dtkFemFiniteElementRCTTriangle(const std::vector<dtkFemIntegrationPoint *>& gauss_pts)
//{
//
//}

dtkFemFiniteElementRCTTriangle::dtkFemFiniteElementRCTTriangle(std::size_t elt_id, const dtkDistributedMesh& mesh, const std::vector<dtkFemIntegrationPoint *>& gauss_pts) : dtkFemFiniteElement()
{

}

dtkFemFiniteElementRCTTriangle::~dtkFemFiniteElementRCTTriangle(void)
{
}

void dtkFemFiniteElementRCTTriangle::computeEccentricity(void)
{
    double l12, l23, l31;

    l12  = (m_coord(1,0) - m_coord(0,0)) * (m_coord(1,0) - m_coord(0,0));
    l12 += (m_coord(1,1) - m_coord(0,1)) * (m_coord(1,1) - m_coord(0,1));

    l23  = (m_coord(2,0) - m_coord(1,0)) * (m_coord(2,0) - m_coord(1,0));
    l23 += (m_coord(2,1) - m_coord(1,1)) * (m_coord(2,1) - m_coord(1,1));

    l31  = (m_coord(0,0) - m_coord(2,0)) * (m_coord(0,0) - m_coord(2,0));
    l31 += (m_coord(0,1) - m_coord(2,1)) * (m_coord(0,1) - m_coord(2,1));

    m_ecc[0] = (l12 - l31) / l23;
    m_ecc[1] = (l23 - l12) / l31;
    m_ecc[2] = (l31 - l23) / l12;
}

void dtkFemFiniteElementRCTTriangle::computeBasisLocalDof(void)
{
    m_dof_loc.resize(9,9);

    double A11, A12, A21, A22;
    double B11, B12, B21, B22;
    double C11, C12, C21, C22;

    double X1[2], X2[2], X3[2];

    X1[0] = m_coord(0,0);
    X1[1] = m_coord(0,1);

    X2[0] = m_coord(1,0);
    X2[1] = m_coord(1,1);

    X3[0] = m_coord(2,0);
    X3[1] = m_coord(2,1);

    A11 = X3[0]-X1[0];  A12 = X2[0]-X1[0];
    A21 = X3[1]-X1[1];  A22 = X2[1]-X1[1];

    B11 = X1[0]-X2[0];  B12 = X3[0]-X2[0];
    B21 = X1[1]-X2[1];  B22 = X3[1]-X2[1];

    C11 = X2[0]-X3[0];  C12 = X1[0]-X3[0];
    C21 = X2[1]-X3[1];  C22 = X1[1]-X3[1];

    m_dof_loc = { {1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
                  {0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
                  {0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},

                  {0.0, 0.0, 0.0, A11, A12, 0.0, 0.0, 0.0, 0.0},
                  {0.0, 0.0, 0.0, A21, A22, 0.0, 0.0, 0.0, 0.0},

                  {0.0, 0.0, 0.0, 0.0, 0.0, B11, B12, 0.0, 0.0},
                  {0.0, 0.0, 0.0, 0.0, 0.0, B21, B22, 0.0, 0.0},

                  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, C11, C12},
                  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, C21, C22}
    };
}

//
// dtkFemFiniteElementRCTTriangle.cpp ends here

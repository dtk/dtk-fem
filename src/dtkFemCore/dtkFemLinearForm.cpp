// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

/*!
 * \file dtkFemLinearForm.cpp
 * \brief The class dtkFemLinearForm defines the Linear form of a problem
 *  by calculating all the terms  and assemble all the elements.
 * \author IAAGOUBI Ayoub
 */

#include "dtkFemLinearForm.h"
#include "dtkFemLinearTerm.h"
#include "dtkFemFiniteElement.h"
#include "dtkFemMassTerm.h"

#include <dtkDistributed>

// ///////////////////////////////////////////////////////////////////
// dtkFemLinearForm implementation
// ///////////////////////////////////////////////////////////////////

/*!
  * \brief
  * Constructor of class dtkFemLinearForm with finite element space
  *
*/
dtkFemLinearForm::dtkFemLinearForm(const dtkFemFiniteElementSpace& fes) : m_fes(fes)
{

}

/*!
  * \brief
  * Destructor of class dtkFemLinearForm
  *
*/
dtkFemLinearForm::~dtkFemLinearForm(void)
{

}

/*!
  * \brief
  * A term is defined by a label, for example if we want add an integrator term , it is enough to write:
  *
  * dtkFemDomainIntegratorTerm *d = new dtkFemDomainIntegratorTerm(f);
  *
  * b->addTerm(d, "f(x,y)");
  *
  * To initialize the global vector, it is necessary to define a mapper vertex to cell then set the mapper for the dtkVector.
  *
*/
void dtkFemLinearForm::addTerm(dtkFemLinearTerm *term, const QString& label)
{
    QString vectorImpl = "dtkDistributedVectorData";
    dtkVector<double> *vec = dtkLinearAlgebraSparse::vectorFactory::create<double>(vectorImpl);

    if (vec == nullptr) {
        dtkFatal() << Q_FUNC_INFO << " The dtkVector could not be created by the factory.";
    } else {
        vec->resize(m_fes.nbNodes());
        vec->setMapper(m_fes.vtc_mapper());
        m_terms[label] = term;
        m_rhs[label] = vec;
    }

}

/*!
  * \brief
  * This method allows to recover de vector using the same label defined in addTerm
  *
*/
dtkVector<double> *dtkFemLinearForm::rhs(const QString& label) const
{
    return m_rhs[label];
}

/*!
  * \brief
  * To assemble, we loop on the local elements that correspond to each partition.
  *
  * For each element and each term, we compute the linear term and we multiply by a local matrix then add the contributions to construct a global vector.
  *
  * Here we assemble using  a local matrix of size (3,3), it must be adapted to compute a local matrix with size(dofNumber, dofNumber). For Clough-tocher, for example, we use a local matrix (9,9).
*/
void dtkFemLinearForm::assemble()
{
    dtkDistributedCommunicator *comm = dtkDistributed::communicator::instance();

    if (m_terms.empty()) {
        return;
    }

    for (auto *vec : m_rhs) {
        vec->fill(0.0);
    }
    auto elt_it  = m_fes.beginFE();
    auto elt_end = m_fes.endFE();

    std::vector<double> bsolv;

    for (; elt_it != elt_end; ++elt_it) {

        auto term_it  = m_terms.begin();
        auto term_end = m_terms.end();

        auto vec_it  = m_rhs.begin();

        for (; term_it != term_end; ++term_it, ++vec_it) {

            (*term_it)->computeLinearTerm(*(*elt_it), bsolv);

            // i,j,k are the 3 vertex id for each element.
            int i = (*elt_it)->vertex_id()[0];
            int j = (*elt_it)->vertex_id()[1];
            int k = (*elt_it)->vertex_id()[2];

            (*(*vec_it))[i] += bsolv.at(0);
            (*(*vec_it))[j] += bsolv.at(1);
            (*(*vec_it))[k] += bsolv.at(2);

        }

    }

    comm->barrier();
}
//
// dtkFemLinearForm.cpp ends here

// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <blaze/Math.h>
#include <blaze/math/DynamicMatrix.h>

// ///////////////////////////////////////////////////////////////////
// dtkFemMatrix is an alias for blaze column-major dynamic double matrix
// ///////////////////////////////////////////////////////////////////

using blaze::DynamicMatrix;

typedef DynamicMatrix<double, blaze::columnMajor> dtkFemMatrix;

typedef blaze::Submatrix<dtkFemMatrix>         dtkFemMatrixView;
typedef blaze::Submatrix<const dtkFemMatrix>         dtkFemMatrixViewConst;

//
// dtkFemMatrix.h ends here

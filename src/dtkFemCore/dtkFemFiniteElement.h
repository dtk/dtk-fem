// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkFemCoreExport.h>

#include "dtkFemVector.h"
#include "dtkFemMatrix.h"

#include <vector>

class dtkFemIntegrationPoint;
// ///////////////////////////////////////////////////////////////////
// dtkFemFiniteElement interface
// ///////////////////////////////////////////////////////////////////

class  DTKFEMCORE_EXPORT dtkFemFiniteElement
{
public:
             dtkFemFiniteElement(void);
    virtual ~dtkFemFiniteElement(void);

public:
    const std::vector<dtkFemIntegrationPoint*>& integrationPoints(void) const { return m_ipoints; }
          std::vector<dtkFemIntegrationPoint*>& integrationPoints(void)       { return m_ipoints; }

    std::size_t dofNumber(void) const { return m_dof_number; }
    std::size_t elmt_id(void) const { return m_elmt_id; }
    const std::vector<int>& vertex_id(void) const { return m_vertex_id; }
    const dtkFemMatrix& coord(void) const { return m_coord; }

    virtual const dtkFemVector& basisFunctionValues(std::size_t pi) const = 0;
    virtual const dtkFemMatrix& gradBasisFunctionValues(std::size_t pi) const = 0;
    virtual const double        measure(void) const = 0;

protected:
    std::vector<int> m_vertex_id;
    dtkFemMatrix     m_coord;
    std::size_t      m_dof_number;
    std::size_t      m_elmt_id;
    std::vector<dtkFemIntegrationPoint*> m_ipoints;
};

//
// dtkFemFiniteElement.h ends here
